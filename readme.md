## Local Emulator Set Up
1. Install [firebase-tools](https://www.npmjs.com/package/firebase-tools)
2. Copy `firebase-emu-data-sample` and rename to `firebase-emu-data`
3. Run `npm install` in the functions directory
4. Run `firebase emulators:start` in the root directory or `npm run serve` in the functions directory
5. (Optional) Import `insomnia.json` into Insomnia to test if emulator is working