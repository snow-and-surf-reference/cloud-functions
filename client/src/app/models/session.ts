import { Customer } from "@tsanghoilun/snow-n-surf-interface/types/user";

export type PlayType = "snow" | "ski" | "surf";
export type SessionType =
  | "single"
  | "groupClass"
  | "privateClass"
  | "privateHire";

export type StatusType =
  | "locked"
  | "pending"
  | "confirmed"
  | "cancelled"
  | "refunded";

export interface ISession {
  id: string;
  sessionType: SessionType;
  groupClass: IGroupClass[] | null;
  bookingNumber: string;
  checkedInUsers: Customer[];
  start: number;
  end: number;
  gears: BookingGear[];
  isCheckedIn: boolean;
  pax: number;
  playType: PlayType;
  bookingUser: Customer;
  waivers: string[];
  confirmedDate: number; // date only
  totalCredit: number; // purchase
  createdAt: number;
  status: StatusType;
  lockedTime: number | null;
  confirmedTime: number | null;
  cancelledTime: number | null;
  refundedTime: number | null;
}
export interface IGroupClass {
  isPrivate: boolean;
  start: number;
  end: number;
  coach: ICoach;
  pax: number; // how many joined
  maxPax: number; // maximum students
}
export interface ICoach {
  id: string;
  name: string;
  availableSlot: number[]; //timetable format??? One whole day? By hour? By week?
}

export interface BookingGear {
  size: number | string;
  gear: Gear;
}

export interface Gear {
  id: string;
  type: "snow" | "ski"; // SURF no need gear rental
  name: string; // “Ski Shoe”
  unit: GearUnit;
  standard: GearStock[];
  premium: GearStock[];
  default?: string | number; // default option in UI
}

export interface GearStock {
  size: number | string;
  qty: number;
}

export interface GearUnit {
  slug: string; //working string like “cm” / “EU”
  name: string; //"cm" | "EU" | "inch"
  type: "metric" | "array";
  minSize?: number;
  maxSize?: number;
  step: number; // init = 1; gear size grow by 1 (cm / inch / mm / etc)
  array?: string[]; // S | M | L | XL...
}
