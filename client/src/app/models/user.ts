export interface IUser {
  name: string;
  email: string;
  dob: number;
  image: string;
  mobile: string;
  isVerified: boolean;
  realName: string;
  realImage: string;
  // PASSTYPE => “daily” | weekly”..
  // membershipCode ? //TBA
  // WALLET
  memberSince: number;
}

export interface ICheckInUser extends IUser {
  id: string;
}

export interface ISignedWaiverUser extends ICheckInUser {
  waiver: string;
}

export interface INewUser extends IUser {
  password: string;
  rePassword: string;
}

export interface IWaiver {
  signature: string;
  date: number; //no need time
}

export interface IOrder {
  waiverConfirmation: boolean; //accept or not - NEED time update
  session: []; //session array
}

// on ipad sign waiver form => change user to Verified
// on booking reconfirm waiver, change to new Date() on every booking.
// if new-user, book session, simpler waiver. <=== ???? simply tab "confirm"
