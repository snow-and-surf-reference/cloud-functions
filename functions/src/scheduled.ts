import dayjs from "dayjs";
import * as functions from "firebase-functions";
import { db } from "./firebase";
import utc = require("dayjs/plugin/utc");
import timezone = require("dayjs/plugin/timezone");

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault("Asia/Hong_Kong");

export const createCounterMonthly = functions.pubsub
  .schedule("0 0 1 * *") //cron tab
  .timeZone("Asia/Hong_Kong")
  .onRun(async context => {
    console.log("refresh staff credits", context);

    const staffsDocs = await db.collection("staffs").get();
    staffsDocs.forEach(async ss => {
      await ss.ref.update({ wallet: { balance: ss.data().allowance } });
    });
  });
