import { Timestamp } from "firebase-admin/firestore";

export const fromDB = (data: any) => {
	if (typeof data === "string") {
		return data;
	}
	const result = {};
	for (const [key, value] of Object.entries(data)) {
		if (typeof value === "undefined" || !value) {
			Object.assign(result, {
				[key]: value
			});
		} else if (value instanceof Timestamp) {
			Object.assign(result, {
				[key]: value.toDate()
			});
		} else if (typeof value === "object") {
			if (Array.isArray(value)) {
				const newValue: any[] = [];
				value.forEach(item => {
					newValue.push(fromDB(item));
				});
				Object.assign(result, {
					[key]: newValue
				});
			} else {
				Object.assign(result, {
					[key]: fromDB(value)
				});
			}
		} else {
			Object.assign(result, {
				[key]: value
			});
		}
	}
	return result;
};

export const toDB = (data: any) => {
	if (typeof data === "string") {
		return data;
	}
	const result = {};
	for (const [key, value] of Object.entries(data)) {
		if (typeof value === "undefined" || !value) {
			Object.assign(result, {
				[key]: value
			});
		} else if (value instanceof Date) {
			Object.assign(result, {
				[key]: Timestamp.fromDate(value)
			});
		} else if (typeof value === "object") {
			if (Array.isArray(value)) {
				const newValue: any[] = [];
				value.forEach(item => {
					newValue.push(toDB(item));
				});
				Object.assign(result, {
					[key]: newValue
				});
			} else {
				Object.assign(result, {
					[key]: toDB(value)
				});
			}
		} else {
			Object.assign(result, {
				[key]: value
			});
		}
	}
	return result;
};