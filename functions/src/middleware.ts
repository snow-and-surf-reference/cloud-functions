import { Request, Response, NextFunction } from "express";
import * as functions from "firebase-functions";
import status from "http-status";

export function logRequest(req: Request, _: Response, next: NextFunction) {
  functions.logger.write({ severity: "DEBUG", message: `${req.method} | ${req.path} | ${JSON.stringify(req.body)}` });
  next();
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export function logError(err: Error, _: Request, res: Response, __: NextFunction) {
  functions.logger.write({ severity: "ERROR", message: err.message });
  return res.status(status.INTERNAL_SERVER_ERROR).send(err.message);
}