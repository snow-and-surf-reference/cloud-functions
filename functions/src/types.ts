import type { Request } from "express";
import * as admin from "firebase-admin";
import type { Timestamp } from "firebase-admin/firestore";
import type { WithID } from "./utils";
import * as SessionTypes from "@tsanghoilun/snow-n-surf-interface/types/session";

export enum AccessPoints {
  SNOW_1,
  SNOW_2,
  SURF
}

export function accessPointToString(accessPoint: AccessPoints) {
  switch (accessPoint) {
    case AccessPoints.SNOW_1:
      return "snow1";
    case AccessPoints.SNOW_2:
      return "snow2";
    case AccessPoints.SURF:
      return "surf";
  }
}

export type CircletRecord = WithID<
  {
    access: {
      start: Timestamp;
      end: Timestamp;
      area: ReturnType<typeof accessPointToString>[];
    }[];
  },
  "customerId" | "sessionId"
>;

export type VerifyRequest = Request<any, any, WithID<{ accessPoint: number }>>;

export type UserRecord = { [room in ReturnType<typeof accessPointToString>]?: Timestamp[]; };

export type CleanSessionCallbackRequest = Request<any, any, ExpirationTaskPayload>;

export interface ExpiringDocumentData extends admin.firestore.DocumentData {
  lockedAt?: admin.firestore.Timestamp;
  sessionDate: admin.firestore.Timestamp;
  playType: SessionTypes.PlayType;
  expirationTask?: string;
}

export interface ExpirationTaskPayload {
  docId: string;
  month: number;
  playType: SessionTypes.PlayType;
}

export interface CounterData extends admin.firestore.DocumentData {
  counters: string[];
}

// export { SessionTypes };