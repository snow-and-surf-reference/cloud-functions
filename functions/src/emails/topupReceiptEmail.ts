import * as TopupTypes from "@tsanghoilun/snow-n-surf-interface/types/topUp";
import dayjs from "dayjs";

export const topupReceiptEmail = (recordData: TopupTypes.TopupOrder) => {
    const {
        createdAt,
        authUser,
        requestId,
        topupPackage,
        paymentMethod,
        orderReference,
        exactAmount
    } = recordData;

    return `<!doctype html>
    <html>
        <head>
        <meta charset="UTF-8">
        <!-- utf-8 works for most cases -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Forcing initial-scale shouldn't be necessary -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Use the latest (edge) version of IE rendering engine -->
        <title>EmailTemplate-Responsive</title>
        <!-- The title tag shows in email notifications, like Android 4.4. -->
        <!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->
        <!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->
    
        <!-- CSS Reset -->
        <style type="text/css">
    /* What it does: Remove spaces around the email design added by some email clients. */
          /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
    html,  body {
        margin: 0 !important;
        padding: 0 !important;
        height: 100% !important;
        width: 100% !important;
    }
    /* What it does: Stops email clients resizing small text. */
    * {
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
    }
    /* What it does: Forces Outlook.com to display emails full width. */
    .ExternalClass {
        width: 100%;
    }
    /* What is does: Centers email on Android 4.4 */
    div[style*="margin: 16px 0"] {
        margin: 0 !important;
    }
    /* What it does: Stops Outlook from adding extra spacing to tables. */
    table,  td {
        mso-table-lspace: 0pt !important;
        mso-table-rspace: 0pt !important;
    }
    /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
    table {
        border-spacing: 0 !important;
        border-collapse: collapse !important;
        table-layout: fixed !important;
        margin: 0 auto !important;
    }
    table table table {
        table-layout: auto;
    }
    /* What it does: Uses a better rendering method when resizing images in IE. */
    img {
        -ms-interpolation-mode: bicubic;
    }
    /* What it does: Overrides styles added when Yahoo's auto-senses a link. */
    .yshortcuts a {
        border-bottom: none !important;
    }
    /* What it does: Another work-around for iOS meddling in triggered links. */
    a[x-apple-data-detectors] {
        color: inherit !important;
    }
    </style>
    
        <!-- Progressive Enhancements -->
        <style type="text/css">
            
            /* What it does: Hover styles for buttons */
            .button-td,
            .button-a {
                transition: all 100ms ease-in;
            }
            .button-td:hover,
            .button-a:hover {
                background: #555555 !important;
                border-color: #555555 !important;
            }
    
            /* Media Queries */
            @media screen and (max-width: 600px) {
    
                .email-container {
                    width: 100% !important;
                }
    
                /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
                .fluid,
                .fluid-centered {
                    max-width: 100% !important;
                    height: auto !important;
                    margin-left: auto !important;
                    margin-right: auto !important;
                }
                /* And center justify these ones. */
                .fluid-centered {
                    margin-left: auto !important;
                    margin-right: auto !important;
                }
    
                /* What it does: Forces table cells into full-width rows. */
                .stack-column,
                .stack-column-center {
                    display: block !important;
                    width: 100% !important;
                    max-width: 100% !important;
                    direction: ltr !important;
                }
                /* And center justify these ones. */
                .stack-column-center {
                    text-align: center !important;
                }
            
                /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
                .center-on-narrow {
                    text-align: center !important;
                    display: block !important;
                    margin-left: auto !important;
                    margin-right: auto !important;
                    float: none !important;
                }
                table.center-on-narrow {
                    display: inline-block !important;
                }
                    
            }
    
        </style>
        </head>
        <body bgcolor="#1A1A1A" width="100%" style="margin: 0;" yahoo="yahoo">
        <table bgcolor="#1A1A1A" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
          <tr>
            <td><center style="width: 100%;">
                
                <!-- Visually Hidden Preheader Text : BEGIN -->
                <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;"> Thank You for your Topup. </div>
                <!-- Visually Hidden Preheader Text : END --> 
                
                <!-- Email Header : BEGIN -->
                <table align="center" width="600" class="email-container">
                <tr>
                    <td style="padding: 20px 0; text-align: center"><img src="https://firebasestorage.googleapis.com/v0/b/snow-n-surf-staging.appspot.com/o/email_assets%2Fheader.png?alt=media&token=ca107891-afb0-45ef-96df-fede61ed00e6" width="400" height="" alt="alt_text" border="0"></td>
                  </tr>
              </table>
                <!-- Email Header : END --> 
                
                <!-- Email Body : BEGIN -->
                <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="600" class="email-container">
                
                <!-- Hero Image, Flush : BEGIN -->
                <tr>
                    <td class="full-width-image"><img src="https://firebasestorage.googleapis.com/v0/b/snow-n-surf-staging.appspot.com/o/email_assets%2Fski-bg.jpeg?alt=media&token=f4848b5d-f552-43cf-bb9d-564d13e224b2" width="600" alt="alt_text" border="0" align="center" style="width: 100%; max-width: 600px; height: auto;"></td>
                  </tr>
                <!-- Hero Image, Flush : END --> 
                
                <!-- 1 Column Text : BEGIN -->
                <tr>
                    <td style="padding: 40px; text-align: start; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                        Dear ${authUser.usrname},
                        <br/><br/>
                        Thank you for your topup of ${exactAmount ? exactAmount : topupPackage?.receivedCredits} credits. This email serves as your order invoice and payment receipt.
                        <br>
                    </td>
                  </tr>
                    
                <tr>
                    <td style="padding: 40px; text-align: start; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                        <h2>Your Order</h2>
                        <div>
                            <p><b>Order Date:</b> ${dayjs(createdAt).format("hh:mmA, DD MMM YYYY")}</p>
                            <p><b>Topup Account#:</b> ${authUser.memberId}</p>
                            ${topupPackage ? `<p><b>Package Amount:</b> ${topupPackage.initCredits}</p>`: ""}
                            <p><b>Rceiving Credits:</b> ${topupPackage ? topupPackage.receivedCredits : exactAmount || 0}</p>
                            <p><b>Total Paid:</b> HKD $${topupPackage ? topupPackage.receivedCredits : exactAmount || 0}</p>
                            ${!paymentMethod ? "" : `<p><b>Payment Method:</b> ${paymentMethod === "online_cc" ? "Credit Card (Online)" : paymentMethod === "cc" ? "Credit Card" : "Cash"}</p>`}
                            <p><b>Order Reference:</b> ${orderReference}</p>
                            ${requestId ? `<p><b>Online Transaction ID:</b> ${requestId}</p>` : ""}
                        </div>
                    </td>
                  </tr>
                
                <tr>
                    <td style="padding: 40px; text-align: start; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                        Should you have any enquiry regarding to your purchase, please contact <a href="mailto:info@snownsurfhk.com">info@snownsurfhk.com</a>
                    </td>
                  </tr>
              </table>
                <!-- Email Body : END --> 
                
                <!-- Email Footer : BEGIN -->
                <table align="center" width="600" class="email-container">
                <tr>
                    <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;"><webversion style="color:#cccccc; text-decoration:underline; font-weight: bold;">View as a Web Page</webversion>
                    <br>
                    <br>
                    Snow & Surf HK<br>
                    <span class="mobile-link--footer">snownsurfhk.com</span> <br>
                    <br>
                   </td>
                  </tr>
              </table>
                <!-- Email Footer : END -->
                
              </center></td>
          </tr>
        </table>
    </body>
    </html>
    `;
};