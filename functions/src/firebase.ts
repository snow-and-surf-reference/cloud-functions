import admin from "firebase-admin";

export const firebaseApp = admin.initializeApp();
export const db = admin.firestore(firebaseApp);
export const storage = admin.storage(firebaseApp);