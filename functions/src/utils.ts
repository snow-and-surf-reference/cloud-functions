import type { FirestoreDataConverter, DocumentData, QueryDocumentSnapshot } from "firebase-admin/firestore";

export function getGenericConverter<T>(): FirestoreDataConverter<T> {
  return {
    toFirestore(data: T): DocumentData {
      return data;
    },
    fromFirestore(snapshot: QueryDocumentSnapshot): T {
      return snapshot.data() as T;
    }
  };
}

export type WithID<T, K extends string = "id"> = T & { [key in K]: string };