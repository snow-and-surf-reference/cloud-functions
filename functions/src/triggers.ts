import * as functions from "firebase-functions";
import { CloudTasksClient } from "@google-cloud/tasks";

import dayjs from "dayjs";
import utc from "dayjs/plugin/utc";
import timezone from "dayjs/plugin/timezone";

import { ExpirationTaskPayload, ExpiringDocumentData } from "./types";
import * as SessionTypes from "@tsanghoilun/snow-n-surf-interface/types/session";
import * as GearTypes from "@tsanghoilun/snow-n-surf-interface/types/gear";
import * as UserTypes from "@tsanghoilun/snow-n-surf-interface/types/user";
import * as TopupTypes from "@tsanghoilun/snow-n-surf-interface/types/topUp";
import { FieldValue } from "firebase-admin/firestore";
import { fromDB } from "./helpers";
import { getBookingConfirmationEmail } from "./emails/bookingConfirmation";
import { db } from "./firebase";
import { storageReceiptEmail } from "./emails/storageReceiptEmail";
import { topupReceiptEmail } from "./emails/topupReceiptEmail";
const nodemailer = require("nodemailer");

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault("Asia/Hong_Kong");

export const onCreateSession = functions.firestore.document("/sessions/{id}").onCreate(async ss => {
  console.log("onCreateSession > started");
  const { lockedAt, sessionDate, playType } = ss.data()! as ExpiringDocumentData;
  const status: SessionTypes.StatusType = ss.data().status;
  // const staff = ss.data().bookingStaff;

  if (status !== "locked") {
    return;
  }

  let expirationAtSeconds: number | undefined;
  if (lockedAt) {
    expirationAtSeconds = lockedAt.seconds;
  }

  const project = JSON.parse(process.env.FIREBASE_CONFIG!).projectId;
  const location = "asia-east2";
  const queue = "clean-sessions";

  const tasksClient = new CloudTasksClient();
  const queuePath: string = tasksClient.queuePath(project, location, queue);
  const url = `https://${location}-${project}.cloudfunctions.net/api/clean-session-callback`;
  console.log("from onCreateSession:", project, url);

  const docId = ss.id;
  const month = dayjs.tz(sessionDate.toMillis()).startOf("month").valueOf();
  const payload: ExpirationTaskPayload = {
    docId,
    month,
    playType
  };

  const task = {
    httpRequest: {
      httpMethod: "POST",
      url,
      body: Buffer.from(JSON.stringify(payload)).toString("base64"),
      headers: {
        "Content-Type": "application/json"
      }
    },
    scheduleTime: {
      seconds: expirationAtSeconds
    }
  } as const;

  const [response] = await tasksClient.createTask({
    parent: queuePath,
    task
  });

  const expirationTask = response.name!;
  console.log("onCreateSession > expirationTask", expirationTask);
  await ss.ref.update({ expirationTask });

  console.log("onCreateSession > finished");
});

export const onUpdatePostCancelExpirationTask = functions.firestore
  .document("/sessions/{id}")
  .onUpdate(async change => {
    const before = change.before.data() as ExpiringDocumentData;
    const after = change.after.data() as ExpiringDocumentData;

    // Did the document lose its expiration?
    const expirationTask = after.expirationTask;
    const removedLockedAt = before.lockedAt && !after.lockedAt;
    const status = change.after.data().status as SessionTypes.StatusType;
    const email = String(change.after.data().bookingUser.email);

    if (status === "confirmed" && removedLockedAt) {
      console.log("will cancel the deleteion task");
      const tasksClient = new CloudTasksClient();
      await tasksClient.deleteTask({ name: expirationTask });
      await change.after.ref.update({
        expirationTask: FieldValue.delete()
      });

      const sessionData: SessionTypes.Session = fromDB(change.after.data()) as SessionTypes.Session;
      console.log(sessionData);

      const transporter = nodemailer.createTransport({
        host: "smtp.improvmx.com",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: process.env.SMTP_BOOKING_USRNAME, // generated ethereal user
          pass: process.env.SMTP_BOOKING_PW // generated ethereal password
        }
      });

      const info = await transporter.sendMail({
        from: `[Booking] Snow & Surf HK <${process.env.SMTP_BOOKING_USRNAME}>`, // sender address
        to: email, // list of receivers
        subject: `Booking #${sessionData.bookingNumber} Confirmation`, // Subject line
        text: "Thank You for your Booking", // plain text body
        html: getBookingConfirmationEmail(sessionData)
      });

      console.log("Message sent: %s", info.messageId);

    } else {
      console.log("will remain cancelling if there are task");
    }
  });

//function for onCreate storage orders
export const sendStorageReceipt = functions.firestore
  .document("/storage/{id}")
  .onCreate(async ss => {
    const id = ss.id;
    const data = fromDB(ss.data());
    const storage: GearTypes.GearStorage = { id, ...data } as GearTypes.GearStorage;
    const customerDoc = await db.doc(`customers/${storage.customerId}`).get();
    const customerId = customerDoc.id;
    const customerData = fromDB(customerDoc.data());
    const customer: UserTypes.Customer = {
      id: customerId,
      ...customerData
    } as UserTypes.Customer;

    const transporter = nodemailer.createTransport({
      host: "smtp.improvmx.com",
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: process.env.SMTP_BOOKING_USRNAME, // generated ethereal user
        pass: process.env.SMTP_BOOKING_PW // generated ethereal password
      }
    });

    const info = await transporter.sendMail({
      from: `[Gear Storage] Snow & Surf HK <${process.env.SMTP_BOOKING_USRNAME}>`, // sender address
      to: customer.email, // list of receivers
      subject: "Gear Storage (Annual Rental) Receipt", // Subject line
      text: "Thank you for renting our Gear Storage.", // plain text body
      html: storageReceiptEmail(customer, storage)
    });

    console.log("Message sent: %s", info.messageId);
  });

export const sendTopupReceipt = functions.firestore
  .document("/topUpRecords/{id}")
  .onCreate(async ss => {
    const id = ss.id;
    const data = fromDB(ss.data());
    const order: TopupTypes.TopupOrder =
      { id, ...data } as TopupTypes.TopupOrder;
    
    // check if paymentStatus is AUTHORIZED, 
    // if it's not, DON'T send email.
    if (order.paymentStatus !== "AUTHORIZED") {
      return;
    }

    const transporter = nodemailer.createTransport({
      host: "smtp.improvmx.com",
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: process.env.SMTP_BOOKING_USRNAME, // generated ethereal user
        pass: process.env.SMTP_BOOKING_PW // generated ethereal password
      }
    });

    const amount = order.exactAmount ? order.exactAmount : order.topupPackage?.receivedCredits || 0;

    const info = await transporter.sendMail({
      from: `[Top Up Success] Snow & Surf HK <${process.env.SMTP_BOOKING_USRNAME}>`, // sender address
      to: order.authUser.email, // list of receivers
      subject: "Your Top Up Receipt", // Subject line
      text: `Thank you for purchase of ${amount.toLocaleString()} credits.`, // plain text body
      html: topupReceiptEmail(order)
    });

    console.log("Message sent: %s", info.messageId);

  });