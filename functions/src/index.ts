import cors from "cors";
import express from "express";
import status from "http-status";
import { logError, logRequest } from "./middleware";

import { Timestamp } from "firebase-admin/firestore";
import * as functions from "firebase-functions";
import { db } from "./firebase";

import * as SessionTypes from "@tsanghoilun/snow-n-surf-interface/types/session";
import dayjs from "dayjs";
import { createCounterMonthly } from "./scheduled";
import { onCreateSession, onUpdatePostCancelExpirationTask, sendStorageReceipt, sendTopupReceipt } from "./triggers";
import utc = require("dayjs/plugin/utc");
import timezone = require("dayjs/plugin/timezone");

import {
  accessPointToString,
  CircletRecord,
  CleanSessionCallbackRequest,
  UserRecord,
  VerifyRequest
} from "./types";
import { getGenericConverter } from "./utils";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault("Asia/Hong_Kong");

exports.onCreateSession = onCreateSession;
exports.onUpdatePostCancelExpirationTask = onUpdatePostCancelExpirationTask;
exports.createCounterMonthly = createCounterMonthly;
exports.sendStorageReceipt = sendStorageReceipt;
exports.sendTopupReceipt = sendTopupReceipt;

const app = express();
app.use(cors({
  methods: ["GET", "POST"]
}));
app.use(express.json());
app.use(logRequest);
app.use(logError);

app.post("/verify", async (req: VerifyRequest, res) => {
  if (typeof req.body.id !== "string" || typeof req.body.accessPoint !== "number") {
    return res.status(status.BAD_REQUEST).send();
  }

  const accessPoint = accessPointToString(req.body.accessPoint);

  const query = await db
    .collection("circlet")
    .doc(req.body.id)
    .withConverter(getGenericConverter<CircletRecord>())
    .get();

  if (!query.exists) {
    return res.status(status.NOT_FOUND).send();
  }

  const record = query.data()!;
  const now = Date.now();

  if (
    record.access.some(
      session => session.start.toMillis() < now && session.end.toMillis() > now && session.area.includes(accessPoint)
    )
  ) {
    const archiveRecordRef = db
      .collection("archive")
      .doc(record.customerId)
      .withConverter(getGenericConverter<UserRecord>());
    const archiveRecord = await archiveRecordRef.get();

    if (archiveRecord.exists) {
      functions.logger.log(`Updating archive record for user ${record.customerId}`);
      archiveRecordRef.update({
        [accessPoint]: [...(archiveRecord.data()![accessPoint] ?? []), Timestamp.now()]
      });
    } else {
      functions.logger.log(`Creating archive record for user ${record.customerId}`);
      archiveRecordRef.create({ [accessPoint]: [Timestamp.now()] });
    }

    return res.status(status.OK).send();
  } else {
    return res.status(status.FORBIDDEN).send();
  }
});

app.post("/clean-session-callback", async (req: CleanSessionCallbackRequest, res) => {
  const { docId, month, playType } = req.body;

  if (typeof docId !== "string" || typeof month !== "number" || typeof playType !== "string") {
    return res.status(status.BAD_REQUEST).send();
  }

  // get the doc latest first
  const sessionRef = db.doc(`sessions/${docId}`);

  const sessionLatest = await sessionRef.get();
  const sessionStatus = sessionLatest.data()?.status as SessionTypes.StatusType;

  if (sessionStatus === "confirmed") {
    // do not delete the session or its counters
    return res.status(status.OK).send();
  }

  await db.doc(`sessions/${docId}`).delete();

  // counter doc no longer needed
  
  // const monthRange = {
  //   start: Timestamp.fromDate(dayjs.tz(month).startOf("month").toDate()),
  //   end: Timestamp.fromDate(dayjs.tz(month).endOf("month").toDate())
  // };

  // const counterDocs = await db.collection("counters")
  //   .where("month", ">=", monthRange.start)
  //   .where("month", "<=", monthRange.end).get();
  
  // counterDocs.forEach((ss) => {
  //   const counters = ss.data().sessions.filter((x: any) => x.id === docId);
  //   counters.forEach((counter: any) => {
  //     db.doc(ss.ref.path).update({
  //       sessions: FieldValue.arrayRemove(counter)
  //     });
  //   });
  // });
  
  return res.status(status.OK).send();
});

app.get("/", async (req, res) => { 
  res.send("API active");
});

exports.api = functions.region("asia-east2").https.onRequest(app);