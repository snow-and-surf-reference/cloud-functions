module.exports = {
  "root": true,
  "env": {
    "es6": true,
    "node": true
  },
  "extends": [
    "eslint:recommended",
    "plugin:import/errors",
    "plugin:import/warnings",
    "plugin:import/typescript",
    "plugin:@typescript-eslint/recommended"
  ],
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "project": "tsconfig.json",
    "tsconfigRootDir": __dirname,
    "sourceType": "module"
  },
  "ignorePatterns": [
    "/lib/**/*" // Ignore built files.
  ],
  "plugins": ["@typescript-eslint", "import"],
  "rules": {
    "quotes": ["error", "double"],
    "import/no-unresolved": 0,
    "semi": "warn",
    "@typescript-eslint/no-extra-semi": "warn",
    "comma-spacing": "warn",
    "prefer-const": "warn",
    "no-multi-spaces": [
      "warn",
      {
        "ignoreEOLComments": true
      }
    ],
    "object-curly-spacing": [
      "warn",
      "always"
    ],
    "@typescript-eslint/naming-convention": [
      "warn",
      {
        "selector": ["interface", "class"],
        "format": ["PascalCase"]
      }
    ],
    "@typescript-eslint/explicit-module-boundary-types": "off",
    "@typescript-eslint/no-explicit-any": "off",
    "@typescript-eslint/ban-types": [
      "error",
      {
        "extendDefaults": true,
        "types": {
          "{}": false
        }
      }
    ],
    "@typescript-eslint/no-empty-function": "warn",
    "@typescript-eslint/no-empty-interface": "warn",
    "@typescript-eslint/ban-ts-comment": "off",
    "@typescript-eslint/no-non-null-assertion": "off",
    "@typescript-eslint/no-non-null-asserted-optional-chain": "off",
    "@typescript-eslint/no-var-requires": 0
  }
};
